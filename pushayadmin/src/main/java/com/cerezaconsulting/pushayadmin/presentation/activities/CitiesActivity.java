package com.cerezaconsulting.pushayadmin.presentation.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import com.cerezaconsulting.pushayadmin.R;
import com.cerezaconsulting.pushayadmin.core.BaseActivity;
import com.cerezaconsulting.pushayadmin.presentation.fragments.CitiesFragment;
import com.cerezaconsulting.pushayadmin.presentation.presenters.CitiesPresenter;
import com.cerezaconsulting.pushayadmin.utils.ActivityUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by katherine on 28/06/17.
 */

public class CitiesActivity extends BaseActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_back);
        ButterKnife.bind(this);

        toolbar.setTitle("Elige la ciudad");

        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowHomeEnabled(true);

        CitiesFragment fragment = (CitiesFragment) getSupportFragmentManager()
                .findFragmentById(R.id.body);
        if (fragment == null) {
            fragment = CitiesFragment.newInstance(getIntent().getExtras());

            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    fragment, R.id.body);
        }
        new CitiesPresenter(fragment,this);
    }
    @Override
    public boolean onSupportNavigateUp() {
        String daySelected = getIntent().getExtras().getString("daySelected");
        Bundle bundle = new Bundle();
        bundle.putSerializable("daySelected", daySelected);
        next(CitiesActivity.this, bundle,CountriesActivity.class,true);
        return true;
    }
}
